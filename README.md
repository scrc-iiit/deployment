# Smart-City Living Lab Deployments
The deployment setup involves using Docker, GitLab CI/CD, GitLab Container Registries, Portainer, and Apache web server. Docker enables containerization, packaging applications and dependencies into isolated units. GitLab CI/CD automates testing, building, and deployment. GitLab Container Registries store Docker images securely. Portainer offers an intuitive interface for managing Docker environments. Apache web server acts as a reverse proxy, hosting services at predefined URLs. Together, this setup streamlines the deployment process, ensuring efficient and reliable application delivery.

## Table of contents
- [Smart-City Living Lab Deployments](#smart-city-living-lab-deployments)
  - [Table of contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Deployment Workflow](#deployment-workflow)
    - [Steps to configure deployments](#steps-to-configure-deployments)
      - [Docker](#docker)
      - [GitLab CI/CD](#gitlab-cicd)
      - [GitLab Container Registries](#gitlab-container-registries)
      - [Portainer](#portainer)
      - [Apache Web Server](#apache-web-server)
      - [Finalization](#finalization)
    - [Reference links](#reference-links)
  - [Maintainers](#maintainers)

## Introduction
The deployment setup involves using Docker, GitLab CI/CD, GitLab Container Registries, Portainer, and Apache web server. Docker enables containerization, packaging applications and dependencies into isolated units. GitLab CI/CD automates testing, building, and deployment. GitLab Container Registries store Docker images securely. Portainer offers an intuitive interface for managing Docker environments. Apache web server acts as a reverse proxy, hosting services at predefined URLs. Together, this setup streamlines the deployment process, ensuring efficient and reliable application delivery.

- It is important to note that all the services should be able to accomodate deployment on predefined URL enpoints and development should be done accordingly.

## Deployment Workflow
- Developers commit their code to a GitLab repository.
- GitLab CI/CD automatically triggers a pipeline upon new code commits.
- The pipeline uses Docker to build a Docker image of the application.
- The Docker image is then pushed to the GitLab Container Registry for versioning and storage.
- Portainer, connected to the Docker environment, pulls the new image from the registry.
- Portainer deploys the updated container(s) on the server(s).
- The Apache web server, configured with predefined URL endpoints, forwards incoming requests to the appropriate containers hosting the services.

### Steps to configure deployments
#### Docker
- Install Docker on the server(s) where the application will run.
- Create a Dockerfile for your application, specifying the necessary dependencies and configurations.
- Build the Docker image using the Dockerfile.
- Test the image locally to ensure it works as expected.
- Push the Docker image to a container registry (e.g., GitLab Container Registries) for versioning and sharing.
- Useage of .env files are recomended 

#### GitLab CI/CD
- Ensure your application code is in a Git repository hosted on [GitLab](https://gitlab.com/scrc-iiit/).
- Create a .gitlab-ci.yml file in the root of your repository, defining the CI/CD pipeline stages and jobs.
- Define the build, test, and deploy steps for your application in the CI/CD pipeline.
- Commit and push the .gitlab-ci.yml file to trigger the pipeline automatically.

#### GitLab Container Registries
- Configure GitLab CI/CD to push the Docker image to GitLab Container Registries after a successful build.
- After the build it is mandotory to push the images with corresponding tags which should include "latest", branch-name, commit-hash. This is already provisioned in the .gitlab-ci.yml file.
- The versioning of images is done based on commit hash (SHA) on which the pipline build is performed. This is done due to ease, automation and reduced maintainance of versioning scheme.
- Set up access controls and permissions for the container registry to ensure security.

#### Portainer
- Install Portainer on the server(s) or a separate management server.
- Connect Portainer to the Docker environment on the server(s).
- Use the Portainer web interface to manage and deploy containers (this is deployed in port 9000 of the machine), including pulling the Docker image from the registry and creating containers for your services.

#### Apache Web Server
- Install Apache web server on the server(s) or use an existing installation.
- Set up virtual hosts or reverse proxy configurations to map predefined URL endpoints to the corresponding containerized services. This file is situated at ***/etc/apache2/sites-available/default-ssl.conf***
- static files are stored at ***/vol/<name-of-service>*** if required.

#### Finalization
- Test the deployment to ensure all services are accessible through the predefined URLs.
- Monitor the deployed containers and services to detect any issues.
- Consider setting up automatic scaling or load balancing if needed for improved performance and reliability.

### Reference links
- Docker: https://www.docker.com/
- GitLab CI/CD: https://docs.gitlab.com/ee/ci/
- GitLab Container Registries: https://docs.gitlab.com/ee/user/packages/container_registry/
- Portainer: https://www.portainer.io/
- Apache HTTP Server: https://httpd.apache.org/

## Maintainers
Current maintainers:
- Leo Jesvyn Francis [(leojfrancis)](mailto:leojfrancis.now@gmail.com)

