# Projects Discussion, Future Scope and Probable Debug Scenarios
This Document Specifies the future scope for existing deployment of projects and debug scenarios in case of deployment failures in the future.

## Table of contents
- [Projects Discussion, Future Scope and Probable Debug Scenarios](#projects-discussion-future-scope-and-probable-debug-scenarios)
  - [Table of contents](#table-of-contents)
  - [Website](#website)
    - [General Fail Cases](#general-fail-cases)
    - [Future Scope](#future-scope)
  - [Web API](#web-api)
    - [General Fail Cases](#general-fail-cases-1)
  - [Node Management](#node-management)
    - [General Fail Cases](#general-fail-cases-2)
    - [Future Scope](#future-scope-1)
  - [Deployments](#deployments)
    - [Misc Information](#misc-information)
    - [General Fail Cases](#general-fail-cases-3)
    - [Future Scope](#future-scope-2)
  - [Dodac-api](#dodac-api)
    - [General Fail Cases](#general-fail-cases-4)
    - [Future Scope](#future-scope-3)
  - [Summary Dashboard](#summary-dashboard)
    - [Features and Bug fixes](#features-and-bug-fixes)
    - [General Fail Cases](#general-fail-cases-5)
    - [Future Scope](#future-scope-4)
  - [Reference links](#reference-links)
  - [Maintainers](#maintainers)

## Website
The website is a static site generator using 11ty and Express to trigger builds over HTTP. This is dependent on web-api project to serve API data like staff information etc to the builder to perform HTML builds on corresponding data available through the API.

### General Fail Cases
Most of the issues from the website a deployment problems. It can be due to a cache or unable to reach source files from docker.
- Cache Issues
  - Performing a hard refresh should solve the issue
- Data not shown on the website
  - This can be due to wrong builds from the website builder, API not being available during the build, or the builder not being able to access the API

### Future Scope
- Other parts of the website need to be changed to APIs and Markdown files for faster and non-code changes to the website

## Web API
This Django application uses Django-rest-framework (DRF) to create RESTful APIs. The data is input into the system using the Django admin interface and stored in an SQLite DB. This is used to pass data to the website builder in real-time.

### General Fail Cases
- CORS
  - Settings related to cors need to be updated in the Django project
- API 500 HTTP Code
  - The code has lines where there is an error and that is not handled.

## Node Management
This Django application uses Django-rest-framework (DRF) to create RESTful APIs. The data is input into the system using the Django admin interface and stored in an SQLite DB. This allows the user to create nodes and manage them directly in the Om2m middleware without using any separate API client. This is available to be controlled using REST APIs too. The output of the file can be the creation and management of Entities and the generation of existing config file format for backward compatibility.

### General Fail Cases
- Creation of OneM2M entities is not found
  - There can be a failed connection to onem2m entities. logs for errors are collected and should be used to debug the issue
  - Possibility of mismatch of rules in the creation of entities.
### Future Scope
- An easier-to-understand UI can be made and use APIs to create the required Entities in the onem2m instance.

## Deployments
The deployment setup involves using Docker, GitLab CI/CD, GitLab Container Registries, Portainer, and Apache web server.

### Misc Information
- The IIIT network is controlled using Power DNS and nginx to reverse proxy to the corresponding servers internally by the IT department to which we have no access too.
- Only ports 80(HTTP) and 443(HTTPS) are being reverse proxied to the public network from the firewall. These ports can only be accessed using the corresponding domain names.
- the decision to use Subdomains to reverse proxy SCRC services was made in between. But, The IIIT firewall was not designed to accommodate this change and wild card DNS entry access was not given due to security concerns.
- Thus usage of the URL endpoint for deployments of services was decided.
- It is important to note that all the services should be able to accommodate deployment on predefined URL endpoints and development should be done accordingly.

### General Fail Cases
- Postgress stops working due to exhaustion of memory limit.
  - This can be caused by to memory exhaustion due to unused old docker images.
  - These images are required to be removed from the system depending on the demand.
 
### Future Scope
- The pipelines can be done using Jenkins Pipelines for higher control
- Nginx can be used to upgrade to version 2 of the web server with clean implementation for the summary dashboard.
- A Content Delivery Network (CDN) can be implemented and host static files rather than using a folder.
- docker swarm or K8 can be used for high-load deployments.
- Monitoring of services using a tool like uptime kuma etc needs to be done to have service management and downtime
- inculcating a staging server and a dev server to test the development should be done.
- Usage of programming best practices like issues creation and updation, merge request creation etc, should be maintained and done.
- Usage of secret management software needs to be implemented to handle environment variables, deployment keys and login credentials of servers.

## Dodac-api
The deployment setup involves using Docker, Fast API a Python library. Docker enables the containerization and packaging of applications and dependencies into isolated units. Fast APT enables the user to create API endpoints to expose data to other services.
### General Fail Cases
- There can be a loss of connection to The border router due to IP change or service failure. This can be resolved by checking the corresponding error messages
 
### Future Scope
- The Dodac-API uses a simple background service to host the API endpoint on the border router we can change that and use docker for final deployments.
- The docker is not made to be built on RPI (ARM machines) this change needs to be accommodated so that we can integrate it with the CI-CD pipeline.


## Summary Dashboard
The deployment setup involves using Django, Django rest framework, Redis, Huey, and Docker. This service is responsible for the dashboards and the data storage of nodes from om2m to Postgres.

### Features and Bug fixes
- Flattened the folder structure of the project
- Dockerised the below services
  - Redis message broker.
  - Huey service
- Added environment variables and streamlined settings for the project.
- Collection of data from the om2m server is done using the Gevents python library using green threads
- The time interval of collection of data was changed to 10min

### General Fail Cases
- Connection to Postgres needs to be checked, This includes Postgres not being down. As Postgres is not dockerised this service needs to be maintained separately.
- Grafana is deployed using an iframe which is deployed using docker separately This also needs to be maintained so that the graphs are served without interruptions

 
### Future Scope
- The Dodac-API uses a simple background service to host the API endpoint on the border router we can change that and use docker for final deployments.
- The docker is not made to be built on RPI (ARM machines) this change needs to be accommodated so that we can integrate it with the CI-CD pipeline.

## Reference links
- Website: https://gitlab.com/scrc-iiit/website
- Website API: https://gitlab.com/scrc-iiit/api-website
- Node Management: https://gitlab.com/scrc-iiit/node-management
- Dodag API: https://gitlab.com/scrc-iiit/dodac-api

## Maintainers
Current maintainers:
- Leo Jesvyn Francis [(leojfrancis)](mailto:leojfrancis.now@gmail.com)